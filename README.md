GET
```
localhost:8080/polygons
localhost:8080/polygons/:name
localhost:8080/polygons/generate

```
POST
```
localhost:8080/polygons
{
    "points":[{"x":1, "y":2},{"x":1, "y":5},{"x":5, "y":5},{"x":5, "y":2}],
    "name": "rect"
}
```